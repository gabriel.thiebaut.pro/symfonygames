<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, array(
                'label' => "Titre",
            ))
            ->add('description', TextareaType::class)
            ->add('image')
            ->add('url')
            ->add('Type', EntityType::class, array(
                'class'=>Type::class,
                'choice_label'=>'title',
                'multiple'=>true,
                'expanded'=>true,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }
}
