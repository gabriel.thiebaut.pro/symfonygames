<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\User;
use App\Entity\Rates;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RatesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('comment')
            ->add('note')
            ->add('approved')
            // ->add('userid', EntityType::class, array(
            //     'class'=>User::class,
            //     'choice_label'=>'email',
            //     'multiple'=>true,
            //     'expanded'=>true,
            // ))
            // ->add('gameid', EntityType::class, array(
            //     'class'=>Game::class,
            //     'choice_label'=>'title',
            //     'multiple'=>true,
            //     'expanded'=>true,
            // ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Rates::class,
        ]);
    }
}
