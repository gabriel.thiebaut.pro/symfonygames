<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\GameRepository;
use App\Repository\TypeRepository;
// use App\Repository\RatesRepository;
use App\Entity\User;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Game;
use App\Form\FavoritesType;

class FavoritesController extends AbstractController
{
    #[Route('/favorites', name: 'favorites')]

    public function index(GameRepository $gameRepository, TypeRepository $typeRepository, UserRepository $userRepository): Response
    {
        $userone = $this->getUser();
        $id = $userone->getId();
        $activeuser = $userRepository->findOneBy(['id'=> $id]);

        return $this->render('favorites/index.html.twig', [
            'games'=>$gameRepository->findAll(),
            'types' => $typeRepository->findAll(),
            'user' => $userone,
            'favorites' => $activeuser->getFavorites()->toArray(),
        ]);
    }

    #[Route('/favorites/new/{id}', name: 'favorites_new', methods: ['GET','POST'])]
    public function new(GameRepository $repo, Request $request, $id)
    {
        $userone = $this->getUser();
        
        $game = $repo->find($id);
        $form = $this->createForm(FavoritesType::class, $game);
        $form->handleRequest($request);
        $game->addUser($userone);
        $entitymanager = $this->getDoctrine()->getManager();
        $entitymanager->persist($game);
        $entitymanager->flush();

        return $this->redirectToRoute('favorites');
    }

    #[Route('/favorites/remove/{id}', name: 'favorites_remove', methods: ['GET','POST'])]
    public function remove(GameRepository $repo, Request $request, $id)
    {
        $userone = $this->getUser();
        
        $game = $repo->find($id);
        $form = $this->createForm(FavoritesType::class, $game);
        $form->handleRequest($request);
        $game->removeUser($userone);
        $entitymanager = $this->getDoctrine()->getManager();
        $entitymanager->persist($game);
        $entitymanager->flush();

        return $this->redirectToRoute('favorites');
    }

}