<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Rates;
use App\Form\RatesType;
use App\Repository\RatesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\GameRepository;

#[Route('/rates')]
class RatesController extends AbstractController
{
    #[Route('/', name: 'rates_index', methods: ['GET'])]
    public function index(RatesRepository $ratesRepository): Response
    {
            $idgame = $_GET['id'];
            // dd($idgame);
        return $this->render('rates/index.html.twig', [
            'rates' => $ratesRepository->findBy(array('gameid' => $idgame)),
        ]);
    }

    #[Route('/new/{id}', name: 'rates_new', methods: ['GET','POST'])]
    public function new(Request $request, GameRepository $gameRepository, $id): Response
    {
        $game = $gameRepository->findOneBy(['id' => $id]);
        $rate = new Rates();
        $form = $this->createForm(RatesType::class, $rate);
        $form->handleRequest($request);
        $userid =  $this->getUser();
        if ($form->isSubmitted() && $form->isValid()) {
            $rate->setUserid($userid);
            
            $rate->setDate(new \DateTimeImmutable('now'));
            $rate->setGameid($game);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rate);
            $entityManager->flush();

            return $this->redirectToRoute('games', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('rates/new.html.twig', [
            'rate' => $rate,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'rates_show', methods: ['GET'])]
    public function show(Rates $rate): Response
    {
        return $this->render('rates/show.html.twig', [
            'rate' => $rate,
        ]);
    }

    #[Route('/{id}/edit', name: 'rates_edit', methods: ['GET','POST'])]
    public function edit(Request $request, Rates $rate): Response
    {
        $form = $this->createForm(RatesType::class, $rate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('rates_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('rates/edit.html.twig', [
            'rate' => $rate,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'rates_delete', methods: ['POST'])]
    public function delete(Request $request, Rates $rate): Response
    {
        if ($this->isCsrfTokenValid('delete'.$rate->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($rate);
            $entityManager->flush();
        }

        return $this->redirectToRoute('rates_index', [], Response::HTTP_SEE_OTHER);
    }
}
